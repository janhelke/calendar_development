###############################################################################
#                                VARIABLES                                    #
###############################################################################
.PHONY: *
ARGS = $(filter-out $@,$(MAKECMDGOALS))
TYPO3CMS = ddev exec ./bin/typo3cms
PHPSTAN = ddev exec ./bin/phpstan --configuration=Build/Testing/phpstan.neon

## list: display target descriptions
list: ## Display target descriptions
	@grep '^[[:alnum:]]\S*:.*' Makefile | sed 's/:.*## /#/' | column -t -s'#'

###############################################################################
#                                  APPLICATION                                #
###############################################################################

up: ## Start application containers.
	ddev start

down: ## Stop application containers.
	ddev stop

restart: ## Restart application containers.
	ddev restart

info: ## Get a detailed description of a running application.
	ddev describe

flush: ## Flush TYPO3 caches.
	$(TYPO3CMS) install:generatepackagestates
	$(TYPO3CMS) install:fixfolderstructure
	$(TYPO3CMS) database:updateschema
	$(TYPO3CMS) cache:flush --force
	ddev composer dump-autoload

###############################################################################
#                             TESTING + LINTING                               #
###############################################################################

test: ## Run all working tests.
	$(MAKE) lint
	$(MAKE) test-php-static
	$(MAKE) test-php-unit

test-php-static: ## Run static analysis tool on ext/
	$(PHPSTAN) analyse --no-progress ext/

test-php-unit: ## Run all working PHPunit tests.
	ddev exec ./bin/phpunit --color -d memory_limit=-1 -c ./Build/Testing/UnitTests.xml

###############################################################################
#                              CODE QUALITY                                   #
###############################################################################

lint: ## Run all linters.
	$(MAKE) lint-php
	$(MAKE) lint-editorconfig

lint-php: ## Run the PHP linter.
	ddev exec ./bin/php-cs-fixer fix --dry-run --diff --config ./Web/typo3conf/ext/cal/Build/.php_cs.php

lint-editorconfig: ## Run the editorconfig linter
	ddev exec ./bin/editorconfig-checker --exclude 'mask.json' ./Web/typo3conf/ext/cal/*

fix:
	$(MAKE) fix-php
	$(MAKE) fix-editorconfig

fix-php:
	ddev exec ./bin/php-cs-fixer fix --config ./Web/typo3conf/ext/cal/Build/.php_cs.php

fix-editorconfig:
	ddev exec ./bin/editorconfig-checker --exclude 'mask.json' ./Web/typo3conf/ext/cal/* --auto-fix

###############################################################################
#                                  PROVISIONING                               #
###############################################################################

init: ## Installs the complete application.
	$(MAKE) up
	ddev composer install
	make import-database
	ddev describe

import-database: ## Imports the database from the tar ball stored in the git. Fast import, but probably outdated data
	ddev import-db --src Build/Data/database.sql.gz
	$(TYPO3CMS) database:updateschema
	$(MAKE) flush

###############################################################################
#                                  PIPES                                      #
###############################################################################

composer: ## Run composer within web container. Use this like `make composer require`.
	ddev composer $(ARGS)
	$(MAKE) flush

typo3cms: ## Run typo3cms binary within web container. Use this like `make typo3cms install:fixfolderstructure.`
	$(TYPO3CMS) $(ARGS)

###############################################################################
#                        Argument fix workaround                              #
###############################################################################
%:
	@:

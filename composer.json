{
    "name": "helke/calendar-develop",
    "type": "metapackage",
    "authors": [
        {
            "name": "Jan Helke",
            "email": "j.helke@helke.de"
        }
    ],
    "repositories": [
        {
            "type": "path",
            "url": "ext/*"
        },
        {
            "type": "git",
            "url": "git@gitlab.com:janhelke/calendar_api.git"
        },
        {
            "type": "git",
            "url": "git@gitlab.com:janhelke/calendar_foundation.git"
        },
        {
            "type": "git",
            "url": "git@gitlab.com:janhelke/calendar_frontend.git"
        },
        {
            "type": "git",
            "url": "git@gitlab.com:janhelke/calendar_migration.git"
        },
        {
            "type": "git",
            "url": "git@gitlab.com:janhelke/calendar_demonstration.git"
        }
    ],
    "require": {
        "ext-dom": "*",
        "ext-json": "*",
        "ext-pdo": "*",
        "bk2k/bootstrap-package": "^11.0",
        "friendsoftypo3/typo3db-legacy": "^1.1",
        "janhelke/cal": "^2.0",
        "janhelke/calendar-api": "dev-master",
        "janhelke/calendar-demonstration": "dev-master",
        "janhelke/calendar-foundation": "dev-master",
        "janhelke/calendar-frontend": "dev-master",
        "janhelke/calendar-migration": "dev-master",
        "sjbr/static-info-tables": "^6.7",
        "typo3/cms-base-distribution": "^9.5"
    },
    "minimum-stability": "dev",
    "prefer-stable": true,
    "require-dev": {
        "friendsofphp/php-cs-fixer": "^2.13",
        "phpstan/phpstan": "^0.12",
        "saschaegerer/phpstan-typo3": "^0.12",
        "typo3/cms-lowlevel": "^9.4",
        "typo3/testing-framework": "^1 || ^4 || ^5"
    },
    "config": {
        "bin-dir": "bin",
        "sort-packages": true
    },
    "extra": {
        "typo3/cms": {
            "web-dir": "Web"
        },
        "installer-paths": {
            "./typo3conf/ext/{$name}/": [
                "type:typo3-cms-extension"
            ]
        },
        "helhum/typo3-console": {
            "install-binary": false,
            "install-extension-dummy": false
        }
    },
    "autoload": {
        "psr-4": {
            "TYPO3\\CMS\\Cal\\": "Web/typo3conf/ext/cal/Classes/"
        }
    },
    "scripts": {
        "post-autoload-dump": [
            "@typo3"
        ],
        "typo3": [
            "./bin/typo3cms database:updateschema",
            "./bin/typo3cms install:fixfolderstructure",
            "./bin/typo3cms install:generatepackagestates",
            "./bin/typo3cms cache:flush"
        ],
        "lint": [
            "@lint:php"
        ],
        "lint:php": [
            "./bin/php-cs-fixer fix --dry-run --diff --config ./Web/typo3conf/ext/calendar_api/Build/.php_cs.php",
            "./bin/php-cs-fixer fix --dry-run --diff --config ./Web/typo3conf/ext/calendar_api_client/Build/.php_cs.php",
            "./bin/php-cs-fixer fix --dry-run --diff --config ./Web/typo3conf/ext/calendar_foundation/Build/.php_cs.php",
            "./bin/php-cs-fixer fix --dry-run --diff --config ./Web/typo3conf/ext/calendar_frontend/Build/.php_cs.php",
            "./bin/php-cs-fixer fix --dry-run --diff --config ./Web/typo3conf/ext/calendar_icalendar/Build/.php_cs.php",
            "./bin/php-cs-fixer fix --dry-run --diff --config ./Web/typo3conf/ext/calendar_migration/Build/.php_cs.php",
            "./bin/php-cs-fixer fix --dry-run --diff --config ./Web/typo3conf/ext/calendar_demonstration/Build/.php_cs.php"
        ],
        "fix:php": [
            "./bin/php-cs-fixer fix --diff --config ./Web/typo3conf/ext/calendar_api/Build/.php_cs.php",
            "./bin/php-cs-fixer fix --diff --config ./Web/typo3conf/ext/calendar_api_client/Build/.php_cs.php",
            "./bin/php-cs-fixer fix --diff --config ./Web/typo3conf/ext/calendar_foundation/Build/.php_cs.php",
            "./bin/php-cs-fixer fix --diff --config ./Web/typo3conf/ext/calendar_frontend/Build/.php_cs.php",
            "./bin/php-cs-fixer fix --diff --config ./Web/typo3conf/ext/calendar_icalendar/Build/.php_cs.php",
            "./bin/php-cs-fixer fix --diff --config ./Web/typo3conf/ext/calendar_migration/Build/.php_cs.php",
            "./bin/php-cs-fixer fix --diff --config ./Web/typo3conf/ext/calendar_demonstration/Build/.php_cs.php"
        ],
        "test": [
            "@lint",
            "@test:php"
        ],
        "test:php": [
            "@test:php-static",
            "@test:php-unit",
            "@test:foundation:php-function",
            "@test:migration:php-function"
        ],
        "test:php-static": "./bin/phpstan --configuration=./Build/Testing/phpstan.neon analyse --no-progress ext/ --level=5",
        "test:php-unit": "./bin/phpunit --color -d memory_limit=-1 -c ./Build/Testing/UnitTests.xml",
        "test:foundation": [
            "@test:foundation:php"
        ],
        "test:foundation:php": [
            "@test:foundation:php-unit",
            "@test:foundation:php-function"
        ],
        "test:foundation:php-unit": "./bin/phpunit --color -d memory_limit=-1 -c ./Build/Testing/UnitTests.xml Web/typo3conf/ext/calendar_foundation/Tests/Unit",
        "test:foundation:php-function": "rm -rf typo3temp/var/tests/functional-*; typo3DatabaseName=\"typo3_functional\" typo3DatabaseHost=\"db\" typo3DatabaseUsername=\"root\" typo3DatabasePassword=\"root\" ./bin/phpunit -d memory_limit=-1 --colors -c ./Build/Testing/FunctionalTests.xml Web/typo3conf/ext/calendar_foundation/Tests/Functional",
        "test:migration": [
            "@test:migration:php"
        ],
        "test:migration:php": [
            "@test:migration:php-unit",
            "@test:migration:php-function"
        ],
        "test:migration:php-unit": "./bin/phpunit --color -d memory_limit=-1 -c ./Build/Testing/UnitTests.xml Web/typo3conf/ext/calendar_migration/Tests/Unit",
        "test:migration:php-function": "rm -rf typo3temp/var/tests/functional-*; typo3DatabaseName=\"typo3_functional\" typo3DatabaseHost=\"db\" typo3DatabaseUsername=\"root\" typo3DatabasePassword=\"root\" ./bin/phpunit -d memory_limit=-1 --colors -c ./Build/Testing/FunctionalTests.xml Web/typo3conf/ext/calendar_migration/Tests/Functional"
    }
}
